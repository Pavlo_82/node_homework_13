import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  JoinTable,
} from 'typeorm';
import { UserEntity } from './user.ts';
import { User } from '@sentry/node';
@Entity()
class NewsPost extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  title!: string;

  @Column()
  text!: string;

  @Column()
  genre!: "Politic" | "Business" | "Sport" | "Other";

  @Column({ default: false })
  isPrivate!: boolean;

  @ManyToOne(type => UserEntity, (user: User) => user.newsPosts)
  @JoinTable()
  author!: UserEntity;

}
export { NewsPost as NewsPostEntity };

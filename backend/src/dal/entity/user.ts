import {
    Entity,
    Column,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany,
    JoinTable,
  } from "typeorm";
  import { NewsPostEntity } from "./newspost.ts";
  
  @Entity()
  class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;
  
    @Column()
    email!: string;
  
    @Column()
    password!: string;
  
    @OneToMany(() => NewsPostEntity, (newPosts) => newPosts.author)
    @JoinTable()
    newPosts!: NewsPostEntity[];
  }
  
  export { User as UserEntity };

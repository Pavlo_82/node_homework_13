import { DataSource } from "typeorm";
import * as Entities from "./entity/index.ts";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "Pavlo04051982",
  database: "postgres",
  synchronize: true,
  logging: true,
  entities: Entities,
  //subscribers: [],
  //migrations: [],
});
